import { Herb } from '../interfaces/herb.interface';

export const getHerbsMock = (): Herb[] => [
  {
    name: 'Lorem ipsum növény',
    name_latin: 'Lorem Construct Latino',
    syn: 'Kiskancsó, Lorem constructor',
    therapy: 'Általános európai terápiákban',
    home:
      'Magyarországon nem megtalálható, de európa számos magashegyi csúcsány megél',
    similar_flower: 'Nincs ilyen',
    harvest_flower: 'Virág, mag',
    dosis: 'Hatóanyaga a lorem ipsum consturct',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque lorem sem, interdum pretium purus quis, blandit maximus erat. Vestibulum et nunc orci. Proin congue, est eu venenatis volutpat, nisl elit aliquam nisi, a sodales nunc lacus vitae eros. Cras lobortis arcu vitae elit porttitor dignissim. Phasellus sodales vitae magna nec malesuada. Cras iaculis vehicula urna, sit amet rhoncus leo ultricies quis. Aliquam justo orci, pharetra non magna ut, viverra tincidunt sapien. Aenean id ipsum pretium, iaculis tellus sit amet, varius neque. Phasellus diam mauris, dapibus et leo id, sagittis pellentesque dui. Nunc viverra urna eget porttitor aliquam. Phasellus ut dui non dolor pellentesque semper. Cras commodo, mi nec laoreet volutpat, urna risus venenatis lectus, id congue ex est non nunc. Pellentesque non pulvinar felis. Fusce iaculis congue nunc, quis maximus arcu aliquam id.',
    sick_flower: 'Fejfájás, vízhajtó',
    sick: 'Lorem, ipsum, fejfájás, máj',
    effects: 'Nincs hatása, csak a fejre lorem',
    use_medicine: 'Főzni kell jó sokat, hogy kinyerjük a lorem ipsumot',
    find_this: 'http://locaiton gps map link',
    side_effects: 'Hasmenést fokozza mellékhatásként',
    harvest_time: 'Tavasz végétől gyűjthető',
    images: ['/flowers/flower.jpg'],
    image_main: '/flowers/flower.jpg',
    background: '/flowers/flower.jpg'
  },
  {
    name: 'masik noveny',
    name_latin: 'Latino',
    syn: '',
    therapy: 'Különleges európai terápiákban',
    home:
      'Magyarországon nem megtalálható, de európa számos magashegyi csúcsány megél',
    similar_flower: 'Nincs ilyen',
    harvest_flower: 'Virág, mag',
    dosis: 'Hatóanyaga a lorem ipsum consturct',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque lorem sem, interdum pretium purus quis, blandit maximus erat. Vestibulum et nunc orci. Proin congue, est eu venenatis volutpat, nisl elit aliquam nisi, a sodales nunc lacus vitae eros. Cras lobortis arcu vitae elit porttitor dignissim. Phasellus sodales vitae magna nec malesuada. Cras iaculis vehicula urna, sit amet rhoncus leo ultricies quis. Aliquam justo orci, pharetra non magna ut, viverra tincidunt sapien. Aenean id ipsum pretium, iaculis tellus sit amet, varius neque. Phasellus diam mauris, dapibus et leo id, sagittis pellentesque dui. Nunc viverra urna eget porttitor aliquam. Phasellus ut dui non dolor pellentesque semper. Cras commodo, mi nec laoreet volutpat, urna risus venenatis lectus, id congue ex est non nunc. Pellentesque non pulvinar felis. Fusce iaculis congue nunc, quis maximus arcu aliquam id.',
    sick_flower: 'Fejfájás, vízhajtó',
    sick: 'Lorem, ipsum, fejfájás, máj',
    effects: 'Nincs hatása, csak a fejre lorem',
    use_medicine: 'Főzni kell jó sokat, hogy kinyerjük a lorem ipsumot',
    find_this: 'http://locaiton gps map link',
    side_effects: 'Hasmenést fokozza mellékhatásként',
    harvest_time: 'Tavasz végétől gyűjthető',
    images: ['/flowers/flower.jpg'],
    image_main: '/flowers/flower.jpg',
    background: '/flowers/flower.jpg'
  }
];
