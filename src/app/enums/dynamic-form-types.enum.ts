export enum DynamicFormTypes {
  INPUT = 'input',
  INPUT_ARRAY = 'inputArray',
  TEXTAREA = 'textarea',
  SELECT = 'select'
}
