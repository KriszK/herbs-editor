import { DynamicFormTypes } from '../enums/dynamic-form-types.enum';
import { DynamicForm } from '../interfaces/dynamic-form.interface';

export const dynamicFormConfig = (): DynamicForm[] => [
  {
    key: 'name',
    name: 'Név',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'name_latin',
    name: 'Latin név',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'syn',
    name: 'Hasonló elnevezés',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'therapy',
    name: 'Terápia',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'home',
    name: 'Előfordulás',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'similar_flower',
    name: 'Hasonló növény',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'harvest_flower',
    name: 'Felhasználható része',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'dosis',
    name: 'Hatóanyag',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'description',
    name: 'Leírás',
    type: DynamicFormTypes.TEXTAREA
  },
  {
    key: 'sick_flower',
    name: 'Gyógymód',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'sick',
    name: 'Betegség',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'effects',
    name: 'Hatása',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'use_medicine',
    name: 'Felhasználása',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'find_this',
    name: 'Link',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'side_effects',
    name: 'Mellékhatás',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'harvest_time',
    name: 'Gyűjtési idő',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'images',
    name: 'Kép',

    type: DynamicFormTypes.INPUT_ARRAY
  },
  {
    key: 'image_main',
    name: 'Elsődleges kép',
    type: DynamicFormTypes.INPUT
  },
  {
    key: 'background',
    name: 'Háttér',
    type: DynamicFormTypes.INPUT
  }
];
