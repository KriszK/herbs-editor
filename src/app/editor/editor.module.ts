import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { EditorContainerComponent } from './components/editor-container/editor-container.component';
import { editorRouting } from './editor-routing.module';

@NgModule({
  declarations: [EditorContainerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    editorRouting
  ]
})
export class EditorModule {}
