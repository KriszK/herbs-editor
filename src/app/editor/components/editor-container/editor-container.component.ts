import {Component, OnInit} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import {saveAs} from 'file-saver';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {DynamicFormTypes} from '../../../enums/dynamic-form-types.enum';
import {Herb} from '../../../interfaces/herb.interface';
import {dynamicFormConfig} from './../../../constants/dynamic-form-config';
import {AngularFireStorage} from '@angular/fire/storage';
import 'firebase/storage';

@Component({
  selector: 'app-editor-container',
  templateUrl: './editor-container.component.html',
  styleUrls: ['./editor-container.component.scss']
})
export class EditorContainerComponent implements OnInit {
  herbDataUrl: Observable<string | null>;
  herbs = [];
  generatedJson: Array<any>;
  emptyHerb: Herb = {};

  selectedHerb: Herb;
  herbForm: FormGroup;
  selectedHerbIndex = 0;

  formType = DynamicFormTypes;
  formTest = dynamicFormConfig();

  file: any;
  config: any;

  private objectDestroySource$ = new Subject();

  constructor(private formBuilder: FormBuilder, private storage: AngularFireStorage) {
    this.generateForm();
  }

  ngOnInit() {
    this.herbForm.valueChanges
      .pipe(takeUntil(this.objectDestroySource$))
      .subscribe(value => {
        this.herbs[this.selectedHerbIndex] = value;
      });
  }

  private generateForm(): void {
    this.herbForm = this.formBuilder.group({});
    for (const item of this.formTest) {
      if (item.type === DynamicFormTypes.INPUT_ARRAY) {
        this.herbForm.addControl(
          item.key,
          new FormArray([new FormControl('')])
        );
        this.emptyHerb[item.key] = [''];
      } else {
        if (item.key === 'name') {
          this.herbForm.addControl(
            item.key,
            new FormControl('', Validators.required)
          );
        } else {
          this.herbForm.addControl(item.key, new FormControl(null));
        }
        this.emptyHerb[item.key] = '';
      }
    }
  }

  load(fileInputEvent: any) {
    const self = this;
    const ref = this.storage.ref('data.json');
    this.herbDataUrl = ref.getDownloadURL();

    this.herbDataUrl.subscribe(herbDatas => {
      var oReq = new XMLHttpRequest();
      oReq.onreadystatechange  = function() {
        if (this.readyState == 4 && this.status == 200) {
          self.config = JSON.parse(this.responseText.toString());
          self.herbs = self.config.herbs;
          self.selectedHerbIndex = 0;
          self.updateHerbForm();
        }
      };
      oReq.open('GET', herbDatas);
      oReq.send();
    });
  }

  save() {
    const file = new Blob([JSON.stringify({herbs: this.herbs}, null, '\t')], {
      type: 'application/json'
    });
    const fileName = 'herbs.json';
    console.log(file);
    this.uploadFile(file);
    // saveAs(file, fileName);
  }

  updateHerbForm() {
    this.selectedHerb = this.herbs[this.selectedHerbIndex];

    for (const item of this.formTest) {
      if (item.type === DynamicFormTypes.INPUT_ARRAY) {
        const formArray = this.herbForm.controls[item.key] as FormArray;
        formArray.clear();
        const numberOfFormArrayControls =
          this.selectedHerb[item.key].length - 1;

        for (let i = 0; i <= numberOfFormArrayControls; i++) {
          formArray.push(new FormControl(''));
        }
      }
    }

    this.herbForm.patchValue(this.selectedHerb);
  }

  add(): void {
    this.selectedHerb = this.emptyHerb;
    this.selectedHerb.name = '';
    this.herbs.push(this.selectedHerb);
    this.selectedHerbIndex = this.herbs.length - 1;
    this.updateHerbForm();
  }

  delete() {
    const itemToDeleteIndex = this.herbs.indexOf(this.selectedHerb);
    this.herbs.splice(itemToDeleteIndex, 1);
    this.selectedHerbIndex = 0;
    this.updateHerbForm();
  }

  getArrayControl(key: string): AbstractControl[] {
    const formArray = this.herbForm.controls[key] as FormArray;
    return formArray.controls;
  }

  addNewItem(key: string): void {
    const formArray = this.herbForm.controls[key] as FormArray;
    formArray.push(new FormControl(''));
  }

  deleteItem(key: string, index: number): void {
    const formArray = this.herbForm.controls[key] as FormArray;
    formArray.removeAt(index);
  }

  uploadFile(event) {
    const file = event;
    const filePath = 'data.json';
    const task = this.storage.upload(filePath, file);
  }
}
