import { DynamicFormTypes } from './../enums/dynamic-form-types.enum';

export interface DynamicForm {
  key: string;
  type: DynamicFormTypes;
  values?: string[];
  name: string;
}
