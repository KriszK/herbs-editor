export interface Herb {
  [key: string]: any;
}

// export interface Herb {
//   name: string;
//   name_latin: string;
//   syn: string;
//   therapy: string;
//   home: string;
//   similar_flower: string;
//   harvest_flower: string;
//   dosis: string;
//   description: string;
//   sick_flower: string;
//   sick: string;
//   effects: string;
//   use_medicine: string;
//   find_this: string;
//   side_effects: string;
//   harvest_time: string;
//   images: [
//     {
//       image_url: string;
//     }
//   ];
//   image_main: string;
//   background: string;
// }
